# Disclaimer
picrename is a simple bash-script to rename all .JPG-files in the directory after the scheme YYYY-MM-DD_hh.mm.ss.jpg

picrename is Free Software and all of the source code is licenced under
the GPL, version 3 or higher (at your option).

# Installation
To install picrename copy it to any place defined in PATH. If you are a single user you could put it in ~/bin f.ex.

picrename depends on exiv2.

# Usage

	picrename

The skript doesn't need any arguments because it just works on every file in the working directory.

# Contact

You can contact me for questions and suggestions:

mail: gryps@fripost.org
XMPP: folky@jabber.se
gnusocial: gryps@status.vinilox.eu
